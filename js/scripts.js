var app = app || {};

var spBreak = 767;

app.init = function () {
  app.sliderKv();
  app.spMenu();
  app.tabletViewport();
  app.instagram();
};

app.isMobile = function () {
  return window.matchMedia('(max-width: ' + spBreak + 'px)').matches;
};

app.sliderKv = function () {
  if ($('.js-slick-slider').length) {
    $('.js-slick-slider').slick({
      autoplay: true,
      dots: true,
      arrows: false,
      autoplaySpeed: 2000
    });
  }
  if ($('.js-slider-collection').length) {
    $('.js-slider-collection').slick({
      dots: false,
      arrows: true,
      infinite: true,
      autoplay: false,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
      ]
    });
  }
};

app.tabletViewport = function () {
  var viewport = document.getElementById('viewport');

  var viewportSet = function () {
    if (screen.width >= 768 && screen.width <= 1024) {
      viewport.setAttribute('content', 'width=1280, user-scalable=0');
    } else {
      viewport.setAttribute(
        'content',
        'width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=0'
      );
    }
  };

  viewportSet();

  window.onload = function () {
    viewportSet();
  };

  window.onresize = function () {
    viewportSet();
  };
};

app.spMenu = function () {
  var spNav = $('.nav-menu');
  var offsetY = window.pageYOffset;
  var header = $('header');
  $(document).on('click', '.js-button-menu', function () {
    header.stop().toggleClass('is-active');

    if (header.hasClass('is-active')) {
      offsetY = window.pageYOffset;
      $('body').css({
        position: 'fixed',
        top: -offsetY + 'px',
        width: '100%'
      });
      spNav.fadeIn(500);
    } else {
      $('body').css({
        position: 'static',
        top: 'auto'
      });
      $(window).scrollTop(offsetY);
      spNav.fadeOut(500);
    }
    return false;
  });
};

app.instagram = function () {
  var feed = new Instafeed({
    get: 'user',
    target: 'instagram',
    userId: '1981626751',
    accessToken: '1981626751.dab8d59.046b36877d3747dbba46b095032b6715',
    limit: '18',
    template:
      '<a class="trans item-post" href="{{link}}"" target="_blank"><img src="{{image}}" alt="{{caption}}"/></a>',
    after: function () {
      if ($('.js-slider-instagram').length) {
        $('.js-slider-instagram').slick({
          arrows: true,
          infinite: true,
          rows: 2,
          autoplay: false,
          slidesToShow: 4,
          slidesToScroll: 4,
          responsive: [
            {
              breakpoint: 768,
              settings: {
                arrows: false,
                autoplay: true,
                autoplaySpeed: 2000
              }
            },
            {
              breakpoint: 551,
              settings: {
                arrows: false,
                autoplay: true,
                autoplaySpeed: 2000,
                slidesToShow: 3,
                slidesToScroll: 3
              }
            }
          ]
        });
      }
    }
  });
  feed.run();
};

$(function () {
  app.init();
});
